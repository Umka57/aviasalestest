
import XCTest
import Combine
@testable import AviasalesTestApp

final class UnitTestingSearchViewModel: XCTestCase {

    var searchViewModel : SearchViewModel?
    var cancellables = Set<AnyCancellable>()
    
    override func setUpWithError() throws {
        searchViewModel = SearchViewModel(flightsService: FlightsService(networkService: NetworkService(baseURL: "https://nu.vsepoka.ru/api")))
    }

    override func tearDownWithError() throws {
        searchViewModel = nil
    }

    func test_SearchViewModel_loadFlights_flightShouldBeNotEmpty() throws {
        
        //When
        let expectation = XCTestExpectation(description: "Completion called")
        
        searchViewModel?.$flights
            .dropFirst()
            .sink(receiveValue: { result in
                expectation.fulfill()
            })
            .store(in: &cancellables)
        
        searchViewModel?.loadFlights()
        
        // Then
        wait(for: [expectation], timeout: 5)
        
        XCTAssertGreaterThan(searchViewModel?.flights.count ?? 0, 0)
    }
    
    func test_SearchViewModel_loadFlightsWithWrongCredits_shoulBeFalse() throws {
        
        //Given
        searchViewModel?.originCity = City(iata: "LSP", name: "Не город")
        searchViewModel?.destinationCity = City(iata: "SPB", name: "Тест")
        
        //When
        
        let expectation = XCTestExpectation(description: "Error message appears")
        
        searchViewModel?.$alertMessage
            .dropFirst()
            .sink(receiveValue: { returnedMessage in
            expectation.fulfill()
        }).store(in: &cancellables)
        
        searchViewModel?.loadFlights()
        
        //Then
        wait(for: [expectation], timeout: 5)
        XCTAssertTrue(self.searchViewModel?.flights == [])
        XCTAssertTrue(self.searchViewModel!.isShowAlert)
        XCTAssert(self.searchViewModel?.alertMessage == "Ошибка на стороне пользователя")
        
    }

}
