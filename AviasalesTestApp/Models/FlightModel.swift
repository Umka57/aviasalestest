
import Foundation

// MARK: - Flights
struct Flights: Codable {
    let passengersCount: Int?
    let origin, destination: Destination?
    let results: [FlightResult]?

    enum CodingKeys: String, CodingKey {
        case passengersCount = "passengers_count"
        case origin, destination, results
    }
}

// MARK: - Destination
struct Destination: Codable {
    let iata, name: String?
}

// MARK: - Result
struct FlightResult: Codable, Hashable {
    
    let id, departureDateTime, arrivalDateTime: String?
    let price: Price?
    let airline: String?
    let availableTicketsCount: Int?
    
    enum CodingKeys: String, CodingKey {
        case id
        case departureDateTime = "departure_date_time"
        case arrivalDateTime = "arrival_date_time"
        case price, airline
        case availableTicketsCount = "available_tickets_count"
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
    
    static func == (lhs: FlightResult, rhs: FlightResult) -> Bool {
        return lhs.id == rhs.id
    }
}

// MARK: - Price
struct Price: Codable {
    let currency: String?
    let value: Int?
}

// MARK: - City

struct City: Codable {
    var iata: String
    var name: String
}

