
import Foundation

extension String {
    
    /// For changing date format from "2023-09-03 20:25" ->
    /// "20:25" by using (type: .time)
    /// "3 Сент, Пт" by using (type: .date)
    func splitAndFormatDateTime(type: DateTimeType) -> String? {
        let components = self.components(separatedBy: " ")
        
        guard components.count == 2 else {
            return nil
        }
        
        switch type {
        case .date:
            let date = components[0]
            let dateFormatter = DateFormatter()
            dateFormatter.locale = Locale(identifier: "ru_RU")
            dateFormatter.dateFormat = "yyyy-MM-dd"
            guard let date = dateFormatter.date(from: date) else {
                return nil
            }
            
            dateFormatter.dateFormat = "d MMM, E"
            let formattedDate = dateFormatter.string(from: date)
            
            return formattedDate
            
        case .time:
            return components[1]
        }
    }
    
    enum DateTimeType {
        case date
        case time
    }
    
    /// For localizing string (all setings inside)
    func localized() -> String {
        NSLocalizedString(
            self,
            tableName: "Localizable",
            bundle: .main,
            value: self,
            comment: self)
    }
}



