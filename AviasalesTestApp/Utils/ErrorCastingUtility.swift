import Foundation

extension NetworkServiceError {
    
    func toFlightsError() -> FlightsServiceErrors {
        switch self {
        case .incorrectURL, .urlSessionError:
            return .requestError
            case .multipleChoices,
                    .movedPermanently,
                    .temporaryRedirect,
                    .permanentRedirect:
            return .redirectingError
            case .badRequest,
                    .unathorized,
                    .forbidden,
                    .pageNotFound,
                    .conflict:
            return .userSideError
        case .iternalServerError,
                    .notImplemented,
                    .badGateway,
                    .serviceUnavailable,
                    .gatewayTimeout:
            return .serverSideError
        default:
           return .unknownError
        }
    }
    
}
