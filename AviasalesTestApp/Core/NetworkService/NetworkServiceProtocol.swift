import Foundation

protocol NetworkServiceProtocol {
    
    func baseRequest<ResponseType: Decodable>(parameters: [URLQueryItem] ,endpointPath: String,
                                              completionHandler: @escaping (Result<ResponseType, NetworkServiceError>) -> ())
}

