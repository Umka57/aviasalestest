
import Foundation

struct FlightsService: FlightServiceProtocol {
    
    let network: NetworkServiceProtocol
    
    init(networkService: NetworkServiceProtocol) {
        network = networkService
    }
    
    func getFlightsWithParameters(parameters: [String:String],completion: @escaping (Result<[FlightResult]?, FlightsServiceErrors>) -> ()) {
        
        let queryParameters = parameters.map { URLQueryItem(name: $0.key, value: $0.value) }
        
        DispatchQueue.main.async {
            network.baseRequest(parameters: queryParameters,endpointPath: "/search", completionHandler: { (result: Result<Flights, NetworkServiceError>) in
                
                let finalResult: Result<[FlightResult]?, FlightsServiceErrors> = result.map({ return $0.results })
                    .mapError({ return $0.toFlightsError() })
            
                completion(finalResult)
            })
        }
        
    }
                   
}

enum FlightsServiceErrors: Error {
    case decodingError
    case requestError
    case redirectingError
    case userSideError
    case serverSideError
    case unknownError
    
    var description: String {
            switch self {
            case .decodingError:
                return "Проблемы с декодированием полученных данных"
            case .requestError:
                return "Ошибка в отправке запроса"
            case .redirectingError:
                return "Ошибка, ссылка не найдена"
            case .userSideError:
                return "Ошибка на стороне пользователя"
            case .serverSideError:
                return "Ошибка на стороне сервера"
            case .unknownError:
                return "Неизвестная ошибка"
            }
        }
}

protocol FlightServiceProtocol {
    
    func getFlightsWithParameters(parameters: [String: String] ,completion: @escaping (Result<[FlightResult]?, FlightsServiceErrors>) -> ())

}
