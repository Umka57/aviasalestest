
import SwiftUI

struct SearchView: View {
    
    @ObservedObject var searchViewModel : SearchViewModel
    
    var body: some View {
        
        NavigationView {
            VStack {
                
                if searchViewModel.flights.isEmpty {
                    ProgressView()
                        .padding(.bottom, 50)
                    
                    if searchViewModel.alertMessage != "" {
                        
                        Text("Что-то пошло не так")
                            .font(Font.custom("SF Pro Text", size: 17).weight(.bold))
                            .foregroundColor(Color("MainTextColor"))
                        
                        Button(action: {
                            searchViewModel.loadFlights()
                        }, label: {
                            Text("Повторить")
                                .font(Font.custom("SF Pro Text", size: 17).weight(.bold))
                                .multilineTextAlignment(.center)
                                .foregroundColor(Color.white)
                                .padding(20)
                                .background(Color("ButtonColor"))
                                .cornerRadius(10)
                        })
                    }
                    
                } else {
                    
                    VStack {
                        Text("\(searchViewModel.originCity.name) - \(searchViewModel.destinationCity.name)")
                            .font(Font.custom("SF Pro Text", size: 15).weight(.semibold))
                            .multilineTextAlignment(.center)
                            .foregroundColor(Color("MainTextColor"))
                        
                        Text("\(searchViewModel.date), \(searchViewModel.personCount) чел")
                            .font(Font.custom("SF Pro Text", size: 11))
                            .kerning(0.07)
                            .multilineTextAlignment(.center)
                            .foregroundColor(Color("AccentTextColor"))
                    }
                    
                    ScrollView(.vertical, showsIndicators: false){
                        VStack(spacing: 12) {
                            ForEach(searchViewModel.flights, id: \.self) { flight in
                                NavigationLink(destination: {
                                    FlightDetailsView(flight: flight, originCity: searchViewModel.originCity, destinationCity: searchViewModel.destinationCity)
                                }, label: {
                                    SearchResultItemCard(flight: flight, originCity: searchViewModel.originCity, destinationCity: searchViewModel.destinationCity, isSelected: (flight.id == searchViewModel.bestOfferId))
                                })
                            }
                        }.padding(0)
                    }
                }
            }.padding(.horizontal, 16)
                .background(Color("ViewBackgroundColor").ignoresSafeArea(.all))
                .alert(isPresented: $searchViewModel.isShowAlert, content: {
                    Alert(
                        title: Text("Произошла ошибка"),
                        message: Text(searchViewModel.alertMessage),
                        dismissButton: .default(Text("Повторить"), action: {
                            searchViewModel.loadFlights()
                        })
                    )
                })
            
        }
        .frame(width: .infinity, height: .infinity, alignment: .center)
        .background(Color("ViewBackgroundColor").ignoresSafeArea(.all))
        .onAppear(perform: {
            searchViewModel.loadFlights()
        })
    }
}

struct SearchView_Previews: PreviewProvider {
    static var previews: some View {
        SearchView(searchViewModel: SearchViewModel(flightsService: FlightsService(networkService: NetworkService(baseURL: "https://nu.vsepoka.ru/api"))))
    }
}
