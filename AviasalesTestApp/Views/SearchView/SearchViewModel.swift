
import Foundation

final class SearchViewModel: ObservableObject {
    
    @Published var flights: [FlightResult] = []
    private let flightsService: FlightServiceProtocol
    
    var originCity: City
    var destinationCity: City
    var date: String = "3 сентября"
    var personCount: Int = 1
    var bestOfferId: String = ""
    
    @Published var isShowAlert: Bool = false
    @Published var alertMessage: String = ""
    
    init(flightsService: FlightServiceProtocol) {
        self.flightsService = flightsService
        self.originCity = mockOrigin
        self.destinationCity = mockDestination
    }
    
    func loadFlights() {
        
        let parameters = ["origin": self.originCity.iata, "destination": self.destinationCity.iata]
        
        flightsService.getFlightsWithParameters(parameters: parameters, completion: { [weak self] result in
            DispatchQueue.main.async {
                switch result {
                case .success(let flights):
                    self?.flights = flights?.sorted {$0.price?.value ?? 0 < $1.price?.value ?? 0} ?? []
                    self?.bestOfferId = self?.flights.first?.id ?? ""
                    self?.isShowAlert = false
                case .failure(let error):
                    self?.isShowAlert = true
                    self?.alertMessage = error.description
                }
            }
        })
    }
    
}
