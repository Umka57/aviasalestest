//
//  FlightDetailsRow.swift
//  AviasalesTestApp
//
//  Created by Mikhail on 07.07.2023.
//

import SwiftUI

struct FlightDetailsRow: View {
    
    
    var city: City
    var arrivalDateTime: String?
    
    var body: some View {
        HStack {
            
            VStack {
                Text(city.name)
                  .font(Font.custom("SF Pro Text", size: 15).weight(.semibold))
                  .foregroundColor(Color("MainTextColor"))
                  .frame(maxWidth: .infinity, alignment: .leading)
                  .frame(height: 18)
                
                Text(city.iata)
                  .font(Font.custom("SF Pro Text", size: 13))
                  .foregroundColor(Color("AccentTextColor"))
                  .frame(maxWidth: .infinity, alignment: .leading)
                  .frame(height: 16)
                
            }
            
            Spacer()
            
            VStack {
                Text(arrivalDateTime?.splitAndFormatDateTime(type: .time) ?? "Неверный формат строки")
                  .font(Font.custom("SF Pro Text", size: 15).weight(.semibold))
                  .foregroundColor(Color("MainTextColor"))
                  .frame(maxWidth: .infinity, alignment: .trailing)
                  .frame(height: 18)
                
                Text(arrivalDateTime?.splitAndFormatDateTime(type: .date) ?? "Неверный формат строки".localizedCapitalized)
                  .font(Font.custom("SF Pro Text", size: 13))
                  .foregroundColor(Color("AccentTextColor"))
                  .frame(maxWidth: .infinity, alignment: .trailing)
                  .frame(height: 16)
            }
        }
    }
}

struct FlightDetailsRow_Previews: PreviewProvider {
    static var previews: some View {
        FlightDetailsRow(city: mockOrigin, arrivalDateTime: mockFlight.arrivalDateTime ?? "")
    }
}
