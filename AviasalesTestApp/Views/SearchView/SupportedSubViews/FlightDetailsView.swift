
import SwiftUI

struct FlightDetailsView: View {
    
    @Environment(\.presentationMode) var presentationMode
    
    @State var isShowAlert:Bool = false
    var flight: FlightResult
    var originCity: City
    var destinationCity: City
    private var flightPrice: String
    
    init(flight: FlightResult, originCity: City, destinationCity: City) {
        self.flight = flight
        self.originCity = originCity
        self.destinationCity = destinationCity
        self.flightPrice = String(flight.price?.value ?? 0)
    }
    
    var body: some View {
        VStack {
            HStack {
                Button(action: {
                    presentationMode.wrappedValue.dismiss()
                }, label: {
                    HStack(spacing: 0) {
                        Image("ChevronLeft")
                            .resizable()
                            .scaledToFit()
                            .frame(width: 24, height: 32)
                        
                        Text("Все билеты")
                          .font(Font.custom("SF Pro Text", size: 17))
                    }
                })
            }.frame(maxWidth: .infinity, alignment: .leading)
            
            Text("\(flightPrice) ₽")
              .font(Font.custom("SF Pro Display", size: 34))
              .multilineTextAlignment(.center)
              .foregroundColor(Color("MainTextColor"))
              .frame(maxWidth: .infinity, minHeight: 40, maxHeight: 40, alignment: .top)
            
            Text("Лучшая цена за 1 чел")
              .font(Font.custom("SF Pro Text", size: 13))
              .multilineTextAlignment(.center)
              .foregroundColor(Color("MainTextColor"))
              .frame(maxWidth: .infinity, minHeight: 15, maxHeight: 15, alignment: .top)
              .padding(.bottom, 32)
            
            Text("Москва — Санкт-Петербург")
              .font(Font.custom("SF Pro Text", size: 17))
              .foregroundColor(Color("MainTextColor"))
              .frame(maxWidth: .infinity, minHeight: 20, maxHeight: 20, alignment: .topLeading)
              .padding(.horizontal, 20)
            
            FlightDetailsCard(flight: flight, originCity: originCity, destinationCity: destinationCity)
                .padding(16)
            
            Spacer()
            
            Button(action: {
                isShowAlert.toggle()
            }, label: {
                HStack(alignment: .center, spacing: 0) {
                    Text("Купить билет за \(flightPrice) ₽")
                      .font(Font.custom("SF Pro Text", size: 17).weight(.semibold))
                      .foregroundColor(.white)
                      .frame(maxWidth: .infinity, minHeight: 20, maxHeight: 20, alignment: .top)
                }
                .padding(.horizontal, 16)
                .padding(.vertical, 0)
                .frame(width: 359, height: 48, alignment: .center)
                .background(Color("ButtonColor"))
                .cornerRadius(10)
                .shadow(color: .black.opacity(0.08), radius: 8, x: 0, y: 8)
            })
            
        }.frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .top)
        .background(Color("ViewBackgroundColor").ignoresSafeArea(.all))
        .navigationBarHidden(true)
        .alert(isPresented: $isShowAlert, content: {
            Alert(title: Text("Билет куплен за \(flightPrice) ₽"),
                  dismissButton: .default(Text("Отлично")))
        })
    }
}

struct FlightDetailsView_Previews: PreviewProvider {
    static var previews: some View {
        FlightDetailsView(flight: mockFlight, originCity: mockOrigin, destinationCity: mockDestination)
    }
}
