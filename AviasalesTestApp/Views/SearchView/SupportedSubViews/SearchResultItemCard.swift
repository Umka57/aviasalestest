
import SwiftUI

struct SearchResultItemCard: View {
    
    var flight: FlightResult
    let originCity: City
    let destinationCity: City
    
    private let companyImage: String
    var isSelected: Bool = false
    private let flightPrice : String
    
    init(flight: FlightResult, originCity: City, destinationCity: City, isSelected: Bool) {
        self.flight = flight
        self.originCity = originCity
        self.destinationCity = destinationCity
        self.companyImage = flight.airline?.localized() ?? ""
        self.isSelected = isSelected
        self.flightPrice = String(flight.price?.value ?? 0)
    }
    
    var body: some View {
        
        ZStack {
            
            //All card
            VStack {
                
                VStack(spacing: 0) {
                    //Header with ticket price and logo
                    HStack(spacing: 12) {
                        
                        Text("\(flightPrice) ₽")
                            .font(Font.custom("SF Pro Text", size: 19).weight(.semibold))
                            .foregroundColor(Color("PriceInCardColor"))
                            .frame(maxWidth: .infinity, minHeight: 23, maxHeight: 23, alignment: .topLeading)
                        
                        Image("\(companyImage)Logo")
                            .clipped()
                            .scaledToFit()
                            .frame(width: 32, height: 32, alignment: .center)
                        
                    }
                    
                    if flight.availableTicketsCount ?? 0 < 10 {
                        let ticketsLeft = String(flight.availableTicketsCount ?? 0)
                        Text("Осталось \(ticketsLeft) билетов по этой цене")
                            .font(Font.custom("SF Pro Text", size: 13))
                            .foregroundColor(Color("TicketsLeftLabelColor"))
                            .frame(maxWidth: .infinity, minHeight: 16, maxHeight: 16, alignment: .topLeading)
                    }
                    
                }.padding(EdgeInsets(top: 16, leading: 16, bottom: 0, trailing: 16))
                
                //Row with flight details
                VStack(spacing: 12) {
                    
                    FlightDetailsRow(city: originCity, arrivalDateTime: flight.departureDateTime)
                    
                    FlightDetailsRow(city: destinationCity, arrivalDateTime: flight.arrivalDateTime)
                    
                }.padding(EdgeInsets(top: 0, leading: 16, bottom: 16, trailing: 16))
            }.background(Color("CardBackgroundColor"))
                .cornerRadius(10)
                .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .bottom)
            
            if isSelected {
                HStack(alignment: .center, spacing: 0) {
                    Text("Самый дешёвый")
                      .font(
                        Font.custom("SF Pro Text", size: 13)
                          .weight(.semibold)
                      )
                      .multilineTextAlignment(.center)
                      .foregroundColor(.white)
                      .padding(.horizontal, 8)
                      .padding(.vertical, 0)
                      .frame(height: 20, alignment: .leading)
                      .background(Color("LowestPriceLabelColor"))
                      .cornerRadius(100)
                    
                }.frame(maxWidth: .infinity, maxHeight: .infinity ,alignment: .topLeading)
                    .padding(.leading, 8)
            }
        }.frame(width: .infinity, height: isSelected ? ((flight.availableTicketsCount ?? 0 > 10 ) ? 178 : 193) : ((flight.availableTicketsCount ?? 0 > 10 ) ? 168 : 184), alignment: .bottom)
    }
}

struct SearchResultItemCard_Previews: PreviewProvider {
    static var previews: some View {
        VStack {
            SearchResultItemCard(flight: mockFlight, originCity: mockOrigin, destinationCity: mockDestination, isSelected: true)
        }.frame(maxHeight: .infinity)
            .background(Color.gray)
        
    }
}
