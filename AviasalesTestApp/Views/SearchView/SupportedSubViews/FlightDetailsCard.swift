//
//  FlightDetailsCard.swift
//  AviasalesTestApp
//
//  Created by Mikhail on 07.07.2023.
//

import SwiftUI

struct FlightDetailsCard: View {
    
    var flight: FlightResult
    let originCity: City
    let destinationCity: City
    private let companyImage: String
    
    init(flight: FlightResult, originCity: City, destinationCity: City) {
        self.flight = flight
        self.originCity = originCity
        self.destinationCity = destinationCity
        self.companyImage = flight.airline?.localized() ?? ""
    }
    
    var body: some View {
        
        //All card
        VStack {
            //Header with logo and company name
            HStack(spacing: 12) {
                //TODO: Добавить поддержку DarkMode
                Image("\(companyImage)Logo")
                    .clipped()
                    .scaledToFit()
                    .frame(width: 32, height: 32, alignment: .center)
                
                Text(flight.airline ?? "")
                  .font(Font.custom("SF Pro Text", size: 15).weight(.semibold))
                  .foregroundColor(Color("MainTextColor"))
                  .frame(maxWidth: .infinity, minHeight: 18, maxHeight: 18, alignment: .topLeading)
            }.padding(12)
            
            //Row with flight details
            VStack(spacing: 12) {
                
                FlightDetailsRow(city: originCity, arrivalDateTime: flight.departureDateTime)
                
                FlightDetailsRow(city: destinationCity, arrivalDateTime: flight.arrivalDateTime)
                
            }.padding(.horizontal, 16)
                .padding(.bottom, 16)
        }.background(Color("CardBackgroundColor"))
        .cornerRadius(10)
    }
}

struct FlightDetailsCard_Previews: PreviewProvider {
    static var previews: some View {
        VStack {
            FlightDetailsCard(flight: mockFlight, originCity: mockOrigin, destinationCity: mockDestination)
        }.frame(maxHeight: .infinity)
        .background(Color.gray)
    }
}
