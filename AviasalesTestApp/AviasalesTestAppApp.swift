
import SwiftUI

@main
struct AviasalesTestAppApp: App {
    var body: some Scene {
        let networkService = NetworkService(baseURL: "https://nu.vsepoka.ru/api")
        let flightService = FlightsService(networkService: networkService)
        let searchViewModel = SearchViewModel(flightsService: flightService)
        
        WindowGroup {
            SearchView(searchViewModel: searchViewModel)
        }
    }
}
