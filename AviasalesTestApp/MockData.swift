
import Foundation

let mockPrice = Price(currency: "RUB", value: 4627)

let mockFlight = FlightResult(id: "5ba8ca186f264eee5b691ebfc5ad0730", departureDateTime: "2023-09-03 20:25", arrivalDateTime: "2023-09-03 21:45", price: mockPrice, airline: "Аэрофлот", availableTicketsCount: 7)

let mockOrigin: City = City(iata: "MOW", name: "Москва")

let mockDestination: City = City(iata: "LED", name: "Санкт-Петербург")

